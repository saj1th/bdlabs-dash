package app

import (
	"fmt"
)

// Envs
const (
	MODE_DEV    string = "dev"
	MODE_PROD   string = "prod"
	MODE_DEBUG  string = "debug"
	COOKIE_NAME string = "FORECASTER"
)

var SKUS = map[string]string{
	"064cffff790f": "HP J0Q27US ZBOOK 17 WKSTN I7",
	"0ea6c810bae1": "Apple iBook 12.1-Inch Laptop, G4 iBook",
	"11e8e54ec344": "Custom MSI GT80 Titan-009-32GB-2x512",
	"12777436360a": "Hp Zbook 17, Core I7-4900Mq",
	"13d99a1c5a10": "Dell Laptop Latitude 14 Rugged Extreme",
	"16cdeead9d18": "Alienware 18 18.4-Inch Gaming Laptop",
	"1ba2c9316eb6": "LENOVO Lenovo 20Bh004rus Tp",
	"1c72c1fed137": "VTX1000 with EX mics",
	"203bc5cd1961": "Toughbook 19 CF-195HSSXLM",
	"21313ed99f3f": "Apple MacBook Pro MGX82LL/A 13.3-Inch",
	"236aabadbf01": "Panasonic CF-31WB-09LM",
	"23acbbe70644": "Dell Laptop Precision M6700 ",
	"255228890172": "Lenovo ThinkPad Yoga 12.5",
	"2681c7e225e3": "Apple MacBook Pro MD103LL/A 15.4-Inch ",
	"28c5ae44f6ce": "ASUS ROG GL551JM-EH74 15.6",
	"2c7c395d39ea": "MSI GS60 GHOST PRO-064",
	"2cb7fd4f4158": "Fire HD 7, 7",
	"30abffbf46f4": "Samsung Galaxy Tab 4",
	"33dd1a9416d9": "Kindle Fire HDX",
	"348feeab63dc": "Nexus 7 from Google",
	"349878099841": "Samsung Galaxy Tab S 8.4-Inch",
	"375174d3c855": "Nvidia Shield",
	"37de10a42308": "HP Stream 7",
	"38fe41d7641e": "Google Nexus 9",
	"3ce173fd93a3": "Dell Venue 8 ",
	"450b9d4f015a": "ASUS MeMO Pad 7",
	"464294e147c6": "Acer Iconia Tab",
	"4a90ca691194": "Samsung UN65HU8550 65-Inch 4K Ultra HD ",
	"4acf690cdd5c": "Samsung UN65HU7250 Curved 65-Inch ",
	"4fa80381ce10": "Sony XBR55X850B 55-Inch",
	"5115c9ab36b8": "Samsung UN60HU8550 60-Inch 4K Ultra",
	"52ecc75317e4": "LG Electronics 40UB8000 40-Inch",
	"53757fd7d132": "VIZIO P602ui-B3 60-Inch 4K Ultra HD",
	"54f42df9d1af": "Seiki SE39UY04 39-Inch 4K Ultra HD ",
	"55a6e0d0084c": "Toshiba 58L8400U 58-Inch 4K Ultra HD",
	"55f0196d5602": "VIZIO P702ui-B3 70-Inch ",
	"5948a7e2a5f4": "VIZIO P502ui-B1 50-Inch 4K Ultra HD Smart",
	"5effe4083fe7": "LG Electronics 65UF8500 65-Inch 4K",
	"644235071e0d": "Sony XBR49X850B 49-Inch 4K Ultra HD",
	"658da9646f79": "LG Electronics 55UB9500 55-Inch 4K ",
	"66443ce091b1": "Samsung UN105S9 Curved 105-Inch 4K",
	"6a1f03de38a6": "Samsung UN48JU7500 Curved 48-Inch 4K",
	"6aa330dcb7b4": "LG Electronics 84LM9600 84-Inch",
	"6dd0e529d859": "Sony XBR65X900B 65-Inch 4K Ultra HD",
	"6e76d82047e7": "LG Electronics 65UF9500 65-Inch 4K ",
	"72439090a281": "Sony XBR65X850A 65-Inch 4K Ultra HD ",
	"76a57126b913": "Sony XBR65X850A 65-Inch 4K Ultra HD ",
	"795497bb97df": "Sony XBR-55X900B 55 Ultra HD Smart 3D",
	"79be94825a05": "JVC DM65USR Diamond Series 4K",
}

var SKU_PRICES = map[string]float64{
	"064cffff790f": 2709.29,
	"0ea6c810bae1": 3807.43,
	"11e8e54ec344": 1619.53,
	"12777436360a": 9968.41,
	"13d99a1c5a10": 9947.60,
	"16cdeead9d18": 9086.92,
	"1ba2c9316eb6": 6113.40,
	"1c72c1fed137": 8858.29,
	"203bc5cd1961": 1779.91,
	"21313ed99f3f": 7142.04,
	"236aabadbf01": 8383.05,
	"23acbbe70644": 9365.44,
	"255228890172": 729.28,
	"2681c7e225e3": 5225.00,
	"28c5ae44f6ce": 8010.68,
	"2c7c395d39ea": 5338.60,
	"2cb7fd4f4158": 8327.74,
	"30abffbf46f4": 7543.48,
	"33dd1a9416d9": 581.25,
	"348feeab63dc": 1247.40,
	"349878099841": 9516.05,
	"375174d3c855": 3537.26,
	"37de10a42308": 6350.70,
	"38fe41d7641e": 314.23,
	"3ce173fd93a3": 7257.00,
	"450b9d4f015a": 4245.51,
	"464294e147c6": 1855.51,
	"4a90ca691194": 798.45,
	"4acf690cdd5c": 1691.63,
	"4fa80381ce10": 521.44,
	"5115c9ab36b8": 654.73,
	"52ecc75317e4": 9970.34,
	"53757fd7d132": 5890.25,
	"54f42df9d1af": 565.37,
	"55a6e0d0084c": 9153.11,
	"55f0196d5602": 933.74,
	"5948a7e2a5f4": 2271.48,
	"5effe4083fe7": 2167.67,
	"644235071e0d": 5292.71,
	"658da9646f79": 1262.41,
	"66443ce091b1": 4023.65,
	"6a1f03de38a6": 9114.24,
	"6aa330dcb7b4": 2466.03,
	"6dd0e529d859": 1827.41,
	"6e76d82047e7": 2990.55,
	"72439090a281": 7608.13,
	"76a57126b913": 1950.14,
	"795497bb97df": 1587.29,
	"79be94825a05": 8627.01,
}

type Config struct {
	AssetsUrl  string
	UploadPath string
}

func (c Config) String() string {
	return fmt.Sprintf(":%s:", c.AssetsUrl)
}

//NewConfig creates a config object
func NewConfig(assetsurl, uploadPath string) *Config {
	return &Config{AssetsUrl: assetsurl, UploadPath: uploadPath}
}
