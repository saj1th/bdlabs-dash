package services

import (
	"github.com/jrallison/go-workers"
)

// Cassandra service
type Minion struct {
}

// NewMinion returns a minion object
func NewMinion(redisHost string) *Minion {
	// connect to the cluster
	workers.Configure(map[string]string{
		"server":   redisHost, // location of redis instance
		"database": "0",                    // instance of the database
		"pool":     "30",                   // number of connections to keep open with redis
		"process":  "1",                    // unique process id for this instance of workers (for proper recovery of inprogress jobs on crash)
	})
	return &Minion{}
}

func (m *Minion) Enqueue(job string, args interface{}) {
	workers.Enqueue("forecaster", job, args)
}
