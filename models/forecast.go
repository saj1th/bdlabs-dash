package models

import (
	"bitbucket.org/saj1th/bdlabs-dash/app"
	"bitbucket.org/saj1th/bdlabs-dash/services"
	"fmt"
	"github.com/gocql/gocql"
	"time"
)

//Forecast model
type Forecast struct {
	Sku       string
	Product   string
	Date      string
	Volume    float64
	Sale      float64
	NrmVolume float64
	NrmSale   float64
}

func (a *Forecast) String() string {
	return fmt.Sprintf("sku:%s date:%s volume:%v sale:%v", a.Sku, a.Date, a.Volume, a.Sale)
}

func FetchForecast(sku, date string, cs *services.Cassandra) (*Forecast, *app.Msg, error) {
	const (
		FORECAST_FETCH_SQL = "SELECT volume, sale  FROM predictions WHERE sku=? AND date=?"
	)
	var volume, sale, nrmvolume, nrmsale float64
	const layout = "2006-01-02"
	t, _ := time.Parse(layout, date)

	iter := cs.Query(FORECAST_FETCH_SQL, sku, t).Iter()
	ok := iter.Scan(&volume, &sale)
	if !ok {
		return nil, app.NewErrMsg("No Forecast for the selected product on that date"), nil
	}
	if volume < 0 {
		nrmvolume = 0
	} else {
		nrmvolume = volume
	}
	if sale < 0 {
		nrmsale = 0
	} else {
		nrmsale = sale
	}
	return &Forecast{Sku: sku, Product: app.SKUS[sku], Date: date, Volume: volume, Sale: sale, NrmSale: nrmsale, NrmVolume: nrmvolume}, nil, nil
}

func InsertForecastJob(userID, filepath string, cs *services.Cassandra) (string, *app.Msg) {
	const (
		JOB_INSERT_SQL = "INSERT INTO forecaster_jobs (userId, id, filepath, stage) VALUES (?, ?, ?, ?)"
	)
	var id = gocql.TimeUUID().String()
	if err := cs.Query(JOB_INSERT_SQL, userID, id, filepath, "uploaded").Exec(); err != nil {
		return "", app.NewErrMsg(err.Error())
	}
	return id, nil
}

func FetchJobDetails(jobId, userId string, cs *services.Cassandra) (string, *app.Msg) {
	const (
		JOB_FETCH_SQL = "SELECT (stage) FROM forecaster_jobs WHERE id=? AND userId=?"
	)
	var stage string
	iter := cs.Query(JOB_FETCH_SQL, jobId, userId).Iter()

	ok := iter.Scan(&stage)
	if !ok {
		return "", app.NewErrMsg("Invalid Job Id")
	}

	return stage, nil
}
