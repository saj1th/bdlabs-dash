package handlers

import (
	"net/http"

	"bitbucket.org/saj1th/bdlabs-dash/app"
	"bitbucket.org/saj1th/bdlabs-dash/models"
	"bitbucket.org/saj1th/bdlabs-dash/services"
	"github.com/zenazn/goji/web"

	"time"

	"io"
	"os"
)

//ForecastHandler holds the services used for showing the dashboard
type ForecastHandler struct {
	*BaseHandler
	CS *services.Cassandra
	MN *services.Minion
}

func NewForecastHandler(bh *BaseHandler, cs *services.Cassandra, mn *services.Minion) *ForecastHandler {
	return &ForecastHandler{BaseHandler: bh, CS: cs, MN: mn}
}

//ShowForecastForm shows the forecast input form
func (f *ForecastHandler) ShowForecastForm(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	data := f.getTplVars(c)
	data["selForecast"] = "active"

	data["skus"] = app.SKUS
	err := f.RenderTpl(w, "forecast.html", &data)
	if err != nil {
		return app.InternalServerError.SetErr(err.Error())
	}
	return nil
}

//FetchForecast shows the forecast results
func (f *ForecastHandler) FetchForecast(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	data := f.getTplVars(c)

	sku := r.FormValue("sku")
	date := r.FormValue("date")
	forecast, _, _ := models.FetchForecast(sku, date, f.CS)

	data["forecast"] = forecast
	data["selForecast"] = "active"
	data["skus"] = app.SKUS
	data["sku"] = sku
	data["date"] = date

	err := f.RenderTpl(w, "forecast.html", &data)
	if err != nil {
		return app.InternalServerError.SetErr(err.Error())
	}
	return nil
}

//ShowUploadForm shows the sales data upload form
func (f *ForecastHandler) ShowUploadForm(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	errStr := r.FormValue("err")
	data := f.getTplVars(c)
	data["selForecastUpd"] = "active"
	data["errStr"] = errStr
	data["skus"] = app.SKUS
	err := f.RenderTpl(w, "salesdata.upload.html", &data)
	if err != nil {
		return app.InternalServerError.SetErr(err.Error())
	}
	return nil
}

//ShowJobStatus shows the sales data upload form
func (f *ForecastHandler) ShowJobStatus(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	jobId := c.URLParams["id"]
	userID := c.Env["userid"].(string)

	status, _ := models.FetchJobDetails(jobId, userID, f.CS)
	f.Logr.Info(status)
	data := f.getTplVars(c)
	data["selForecastUpd"] = "active"
	data["status"] = status
	data["skus"] = app.SKUS
	err := f.RenderTpl(w, "job.status.html", &data)
	if err != nil {
		return app.InternalServerError.SetErr(err.Error())
	}
	return nil
}

//UploadSalesData uploads the sales data and submit it to the queue
func (f *ForecastHandler) UploadSalesData(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	r.Body = http.MaxBytesReader(w, r.Body, int64(100<<20))
	err := r.ParseMultipartForm(100000)

	// the FormFile function takes in the POST input id file
	file, header, err := r.FormFile("salesDatFile")
	if err != nil {
		f.Logr.Error(err)
		http.Redirect(w, r, "/forecast/update?err=filesize", http.StatusFound)
		return nil
	}
	defer file.Close()

	if string(header.Header["Content-Type"][0]) != "text/csv" {
		http.Redirect(w, r, "/forecast/update?err=filetype", http.StatusFound)
		return nil
	}

	fileName := time.Now().Format(f.Config.UploadPath + "/salesdata.20060102.150405.csv")
	outFile, err := os.Create(fileName)
	if err != nil {
		f.Logr.Error(err)
		http.Redirect(w, r, "/forecast/update?err=filex", http.StatusFound)
		return nil
	}
	defer outFile.Close()

	// write the content from POST to the file
	_, err = io.Copy(outFile, file)
	if err != nil {
		f.Logr.Error(err)
		//TODO: Add proper error code
		http.Redirect(w, r, "/forecast/update?err=filex", http.StatusFound)
		return nil
	}

	userID := c.Env["userid"].(string)
	id, er := models.InsertForecastJob(userID, fileName, f.CS)
	if er != nil {
		f.Logr.Error(er)
		http.Redirect(w, r, "/forecast/update?err=job", http.StatusFound)
		return nil
	}
	f.MN.Enqueue("SparkSubmit", id)
	http.Redirect(w, r, "/forecast/update/"+id, http.StatusFound)
	return nil
}
