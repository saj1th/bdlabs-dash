package handlers

import (
	"net/http"

	"bitbucket.org/saj1th/bdlabs-dash/app"
	"bitbucket.org/saj1th/bdlabs-dash/services"
	"github.com/zenazn/goji/web"
)

//DashboardHandler holds the services used for showing the dashboard
type DashboardHandler struct {
	*BaseHandler
	CS *services.Cassandra
}

func NewDashboardHandler(bh *BaseHandler, cs *services.Cassandra) *DashboardHandler {
	return &DashboardHandler{BaseHandler: bh, CS: cs}
}

//ShowDashboard shows the dashboard
func (d *DashboardHandler) ShowDashboard(c web.C, w http.ResponseWriter, r *http.Request) *app.Err {
	data := d.getTplVars(c)
	data["selDashboard"] = "active"
	err := d.RenderTpl(w, "dashboard.html", &data)
	if err != nil {
		return app.InternalServerError.SetErr(err.Error())
	}
	return nil
}
